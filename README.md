# Guess Numbers

## 內容描述：
        * 執行後會先產出一個四位數的數字，如 1234，每個位數皆不重複
        * 再由使用者於總回合數結束前猜出答案
        * 每輸入一次答案會顯示比對結果，如 1A2B

## 使用工具：
        * Golang
        * Visual Studio Code 2019

## package：
        * main：進行隨機數字與使用者的答案之比對，並輸出比對結果
        * rnd：由 GenerateRandNum() 生成隨機數字，再由 GenerateGuessNum() 將該數字轉成字串型別的 slice