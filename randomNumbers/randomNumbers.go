package rnd

import (
	"math/rand"
	"strconv"
	"time"
)

func GenerateGuessNum() []string {
	var g []string
	g = make([]string, 0) // 用於檢查是否有生成相同數字

	for len(g) < 4 {
		r := GenerateRandNum() // 用於儲存生成的隨機數
		exist := 0             // 是否重複(無：0；有：1)
		for _, n := range g {  // 遍歷比對
			if n == strconv.Itoa(r) {
				exist = 1
				break
			}
		}
		if exist == 0 {
			g = append(g, strconv.Itoa(r))
		}
	}
	return g
}

func GenerateRandNum() int {
	rand.Seed(time.Now().Unix())
	randNum := rand.Intn(10)
	return randNum
}
