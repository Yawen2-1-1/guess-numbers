package main

import (
	rnd "Guess_Numbers/randomNumbers"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// 生成1000 ~ 9999內每位皆不重複的數
	var guess_key []string
	guess_key = rnd.GenerateGuessNum()

	// 合併字串
	var guess string = strings.Join([]string{guess_key[0], guess_key[1], guess_key[2], guess_key[3]}, "")
	fmt.Printf("%s\n", guess)

	// 使用者輸入每回合所猜數字
	var A int = 0
	var B int = 0
	var round int = 1
	var answer int
	fmt.Println("猜數字	? A ? B\n總共 10 回合，若在 10 回合內猜出答案則獲勝")
	for round <= 10 {
		fmt.Printf("Round %d\n請輸入您的答案：", round)
		fmt.Scanf("%d\r", &answer)

		var answer_bit []int
		var answer_key []string
		answer_bit = make([]int, len(guess_key))
		answer_key = make([]string, len(guess_key))
		temp := answer
		i := len(guess_key) - 1
		// 取出使用者的答案之每位數字(倒序儲存)
		for temp != 0 {
			answer_bit[i] = temp % 10
			temp = temp / 10
			i = i - 1
		}

		// 將使用者的答案從數字轉成字串
		for i = 0; i < len(guess_key); i++ {
			answer_key[i] = strconv.Itoa(answer_bit[i])
		}

		// 比對
		for j := 0; j < 4; j++ {
			for i = 0; i < 4; i++ {
				// equal: 0; smaller: -1; larger: +1
				if (strings.Compare(guess_key[j], answer_key[i]) == 0) && (j == i) {
					A = A + 1
				} else if (strings.Compare(guess_key[j], answer_key[i]) == 0) && (j != i) {
					B = B + 1
				}
			}
		}
		fmt.Printf("比對結果：%dA%dB\n\n", A, B)
		if (A == 4) && (B == 0) {
			fmt.Println("猜對了！獲勝！")
			break
		}
		round = round + 1
		if round <= 10 {
			A = 0
			B = 0
		}
	}
	if (A != 4) && (B != 0) {
		fmt.Printf("10回合結束！答案是：%s\n", guess)
	}
}
